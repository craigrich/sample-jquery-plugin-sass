O2-Opt-In
======================

jQuery Plugin for the O2 Carousel: 

* [o2priority.test.lidao2.com](http://o2priority.test.lidao2.com/)

Stack
-------
- Client:     
    * jQuery: http://jquery.com/    
- Tooling: 
    * Yeoman: http://yeoman.io/
    * Bower: http://bower.io/
    * Grunt: http://gruntjs.com/