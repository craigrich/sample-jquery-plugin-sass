/**
 * @package     O2_component
 * @subpackage  Carousel
 * @version     1.6.0
 * @author      Craig Richardson <craig.richardson@mcsaatchi.com>
 * @date        2014-06-23T15:30Z
 */

(function($, window, document, undefined) {

    'use strict';

    var pluginName = 'o2Carousel',
        defaults = {
            'autoplay': true,
            'autoplayDuration': 3000,
            'mobileBreakpoint': 575
        };

    //Main constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {

        //Cached Element to manipulate the carousel
        $el: '',

        //Cached list of carousel items.
        $items: '',

        //The clickable image maps found on each carousel item
        $maps: '',

        //The current carosuel item being displayed
        $activeItem: '',

        //Cached Pagination Menu
        $pagination: '',

        //Sets to true if Modernizr current mobile breakpoint
        isMobile: false,

        //Keeps track of the autoplay interval function, which we clear one a user has interacted with the carousel
        autoplayInterval: '',

        //Keeps track of a users idle time on the carousel, triggering autoplay after a certain interval
        timer: '',

        //Keeps track of idle time
        idleTime: 0,

        //Called on creation
        init: function() {

            this.$el = $(this.element);
            this.$items = this.$el.find('.o2__carousel__item');
            this.$maps = this.$items.find('map area');
            this.$activeItem = this.$items.siblings('.active');
            this.$pagination = this.$el.find('.o2__carousel__pagination');
            this.isMobile = Modernizr.mq('only all and (max-width: 570px)') ? true : false;

            //Polyfills for IE 8
            if (!Modernizr.borderradius) {
                this.setIERoundedCorners();
            }

            //Bypass intro animation if we have a mobile viewport
            if ((this.isMobile)) {
                this.bindActions();
                this.setTimer();
            } else {
                this.startIntro();
            }

        },

        //Iterates through each carousel item, animating them into the start position
        startIntro: function() {
            var self = this;

            //Add a gradient to the carousel halfway through the animation
            this.$el.delay(300).queue(function() {
                $(this).addClass('has-gradient');
            });

            //For each item, transition in original position
            $.each(this.$items, function(index) {
                TweenLite.from(this, 1.3, {
                    x: '-3000px',
                    ease: 'Back.easeOut',
                    delay: index * 0.04,
                    onComplete: function() {
                        if ((index + 1) === self.$items.length) {
                            self.bindActions();
                            self.autoPlay();
                            self.setTimer();
                        }
                    }
                });
            });
        },

        //Bind actions depending on touch capability
        bindActions: function() {
            var self = this,
                touchEvent = Modernizr.touch ? 'touchstart' : 'click mouseenter';

            if (!self.isMobile) {
                self.$items
                    .on(touchEvent, function(event) {
                        self.slideOutReset(event);
                        self.slideOut(event);
                    });

                $('#Map1 area').on(touchEvent, function() {
                    if (self.$activeItem.get(0) === self.$items.eq(0).get(0)) {
                        self.$activeItem = self.$items.eq(1);
                        self.$items.eq(1).click();
                    }
                });
                $('#Map2 area').on(touchEvent, function() {
                    if (self.$activeItem.get(0) === self.$items.eq(1).get(0)) {
                        self.$activeItem = self.$items.eq(2);
                        self.$items.eq(2).click();
                    }
                });

            } else {
                self.bindMobileActions();
            }

            //Reset the timer for autoplay once a user interacts with the carousel
            self.$el.on(Modernizr.touch ? 'touchstart' : 'mousemove', function() {
                clearInterval(self.autoplayInterval);
                self.isPlaying = false;
                self.idleTime = 0;
            });

            //Update layout on resize.            
            window.addEventListener('resize', function() {
                self.updateLayout();
            });
        },


        bindMobileActions: function() {
            var self = this,
                touchEvent = Modernizr.touch ? 'touchstart' : 'click',
                $prev = self.$pagination.children('.prev'),
                $next = self.$pagination.children('.next');

            $next.on(touchEvent, self, function(e) {
                self.nextSlide(e);
                $next.off(touchEvent);
                setTimeout(function() {
                    self.bindMobileActions();
                }, 700);
            });

            $prev.on(touchEvent, self, function(e) {
                self.prevSlide(e);
                $prev.off(touchEvent);
                setTimeout(function() {
                    self.bindMobileActions();
                }, 700);
            });
        },

        //Update layout based upon media query 
        updateLayout: function() {
            var isNowMobile = Modernizr.mq('only all and (max-width: 575px)');
            if (isNowMobile !== this.isMobile) {
                location.reload();
            }
        },

        //Underscores Debounce function 
        debounce: function(func, wait, immediate) {
            var timeout, result;
            return function() {
                var context = this,
                    args = arguments;
                var later = function() {
                    timeout = null;
                    if (!immediate) {
                        result = func.apply(context, args);
                    }
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) {
                    result = func.apply(context, args);
                }
                return result;
            };
        },

        //Iterates through a timer and resets once a user interacts with the carousel
        setTimer: function() {
            var self = this;
            setInterval(function() {
                self.timerIncrement();
            }, 1000);
        },

        //Increment the timer
        timerIncrement: function() {
            this.idleTime = this.idleTime + 1;
            if (this.idleTime > 1) {
                this.autoPlay();
            }
        },

        //Iterates through carousel items given an interval
        autoPlay: function() {
            var self = this;
            if (self.isPlaying) {
                return;
            } else {
                self.isPlaying = true;
            }
            if (!self.isMobile) {

                self.autoplayInterval = setInterval(function() {
                    var $nextItem = self.$activeItem.next('.o2__carousel__item').length ? self.$activeItem.next('.o2__carousel__item') : self.$items.eq(0);
                    $nextItem.click();
                }, this.settings.autoplayDuration);

            } else {
                self.autoplayInterval = setInterval(function() {
                    self.nextSlide({
                        data: self
                    });
                }, this.settings.autoplayDuration);
            }
            self.isPlaying = true;
        },

        // Mobile Onliy - Moves next slide into view
        nextSlide: function(e) {
            var self = e.data,

                slideWidth = self.$items.width(),
                nextSlide = self.$activeItem.next();

            //If not at the end of the carousel            
            if (self.$activeItem.index() !== self.$items.length - 1) {
                TweenLite.to(self.$activeItem, 0.5, {
                    x: 0 - slideWidth
                });
                TweenLite.from(nextSlide, 0.5, {
                    x: slideWidth
                });
                self.$activeItem = nextSlide;
                self.setPagination();
            }

            //Wrap Around to next item
            else {
                nextSlide = self.$items.eq(0);
                TweenLite.to(self.$activeItem, 0.5, {
                    x: 0 - slideWidth,
                    onComplete: function() {
                        TweenLite.set(self.$items, {
                            x: 0
                        });
                        self.$activeItem = nextSlide;
                        self.setPagination();
                    }
                });
                TweenLite.set(nextSlide, {
                    x: 0
                });
                TweenLite.from(nextSlide, 0.5, {
                    x: slideWidth
                });
            }
        },

        // Mobile Onliy - Moves previous slide into view
        prevSlide: function(e) {
            var self = e.data,
                slideWidth = self.$items.width(),
                prevSlide = self.$activeItem.prev();

            //If not at the start of the carousel
            if (self.$activeItem.index() !== 0) {
                TweenLite.to(prevSlide, 0.5, {
                    x: 0
                });
                TweenLite.to(self.$activeItem, 0.5, {
                    x: slideWidth,
                    onComplete: function() {
                        TweenLite.set(self.$activeItem, {
                            x: 0
                        });
                        self.$activeItem = prevSlide;
                        self.setPagination();
                    }
                });
            }

            //Wrap Around to next item
            else {
                prevSlide = self.$items.eq(self.$items.length - 1);

                TweenLite.to(self.$items, 0.5, {
                    x: slideWidth,
                    onComplete: function() {
                        TweenLite.set(self.$items, {
                            x: 0 - slideWidth
                        });
                    }
                });
                TweenLite.set(prevSlide, {
                    x: 0
                });
                TweenLite.from(prevSlide, 0.5, {
                    x: 0 - slideWidth,
                    onComplete: function() {
                        TweenLite.set(prevSlide, {
                            x: 0
                        });
                        self.$activeItem = prevSlide;
                        self.setPagination();
                    }
                });
            }


        },

        //Moves all slides to the left of target slide
        slideOut: function(e) {
            var self = this;

            self.$items.off();
            self.$maps.off();
            setTimeout(function() {
                self.bindActions();
            }, 400);

            this.$activeItem = $(e.currentTarget);

            TweenLite.to($(e.currentTarget).prevAll(), 0.5, {
                x: 0 - (this.$items.width() * 0.7),
                ease: 'Expo.easeInOut',
                delay: (e.type === 'mouseenter') ? 0.2 : 0,
                onComplete: function() {

                }
            });

            TweenLite.to($(e.currentTarget).prevAll().children().children(':not(.o2__carousel__item__content__background)'), 0.5, {
                alpha: 0,
                delay: (e.type === 'mouseenter') ? 0.2 : 0
            });

        },

        //Moves all slides to its original position
        slideOutReset: function(e) {
            var $el = $(e.currentTarget).nextAll();
            $el.push($(e.currentTarget).get(0));
            TweenLite.to($el, 0.5, {
                x: '0',
                ease: 'Expo.easeInOut',
                delay: 0.2
            });
            TweenLite.to($el.children().children(':not(.o2__carousel__item__content__background)'), 0.5, {
                alpha: 1,
                delay: 0.2
            });
        },



        //Set active pip inside pagination
        setPagination: function() {
            this.$pagination.children().removeClass('active')
                .eq(this.$activeItem.index()).addClass('active');
        },

        //Set Rounded Corners for IE8 and below
        setIERoundedCorners: function() {
            if (window.PIE) {
                this.$items.find('a').each(function() {
                    PIE.attach(this);
                });
            }
        },

    };

    $.fn[pluginName] = function(options) {
        this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
        return this;
    };

})(jQuery, window, document);


//Temporary initialiser
jQuery(document).ready(function($) {
    'use strict';
    $('.o2__carousel').o2Carousel();



});